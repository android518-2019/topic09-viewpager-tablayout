/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.android.viewpager

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

/**
 * Provides the appropriate [Fragment] for a view pager.
 */
class SimpleFragmentPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {


    private val weekdays = arrayOf("Mon", "Tues", "Weds", "Thurs", "Fri", "Sat", "Sun")

    override fun getPageTitle(position: Int): CharSequence = weekdays[position]
        // return super.getPageTitle(position)  ??

    override fun getItem(position: Int): Fragment {
        var frag: Fragment? = null
        when (position) {
            0 -> frag = MondayFragment()
            1 -> frag = TuesdayFragment()
            2 -> frag = WednesdayFragment()
            3 -> frag = ThursdayFragment()
            4 -> frag = FridayFragment()
            5 -> frag = SaturdayFragment()
            6 -> frag = SundayFragment()
        }
        kotlin.requireNotNull(frag) { "No matching fragment" }
        return frag
    }

    override fun getCount(): Int = FRAG_COUNT

    companion object {
        private val FRAG_COUNT = 7
    }
}
